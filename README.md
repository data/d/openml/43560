# OpenML dataset: World-of-Warcraft-auction-house-data

https://www.openml.org/d/43560

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

What is World of Wacraft?
According to Wikipedia:

  World of Warcraft (WoW) is a massively multiplayer online role-playing game (MMORPG) released in 2004 by Blizzard Entertainment. It is the fourth released game set in the Warcraft fantasy universe. World of Warcraft takes place within the Warcraft world of Azeroth, approximately four years after the events at the conclusion of Blizzard's previous Warcraft release, Warcraft III: The Frozen Throne. The game was announced in 2001, and was released for the 10th anniversary of the Warcraft franchise on November 23, 2004. Since launch, World of Warcraft has had eight major expansion packs produced for it: The Burning Crusade, Wrath of the Lich King, Cataclysm, Mists of Pandaria, Warlords of Draenor, Legion, Battle for Azeroth and Shadowlands.
  World of Warcraft was the world's most popular MMORPG by player count of nearly 10 million in 2009. The game had a total of over a hundred million registered accounts by 2014. By 2017, the game had grossed over 9.23 billion in revenue, making it one of the highest-grossing video game franchises of all time. At BlizzCon 2017, a vanilla version of the game titled World of Warcraft Classic was announced, which planned to provide a way to experience the base game before any of its expansions launched. It was released in August 2019.

Why is it worth investigating?
The game World of Warcraft has already been a gold mine when it comes to scientific work (see google scholar). From social studies to finance, the world created by Blizzard has given many information about how players behave with their created characters, modelling how they act in real life. In this notebook, I will explore the auction house prices driven by demand over a few months for raid supplies.
What is the meaning of all those strange words?
World of Wacraft as any other game has specific terminology for items that are present in the world. Here is a summary of the specific items and words that will be used while researching this notebook.

PvE - Player versus Environment, gameplay that focuses on fighting computer generated enemies,
PvP - Player versus Player, gameplay that focuses on fighting other players,
Party - A group of 2-5 players, created for participating in PvE or PvP scenarios,
Raid Group - A group of 6-40 players divided in groups of 5, created for participating in PvE or PvP scenarios,
Raid - An instanced dungeon, with several bosses that, proves a challenge for a raid group,
Flask - One hour character stats buff that persists through death,
Potion - A short boost to stats, that increases damage, survivability etc,
Food - Long character stats buff that dissapears on death,
Feast - A table that provides food buffs, but can be used by multiple people in a group.
Money - The amount of money you have. It's divided into gold, silver and copper:
100 copper = 1 silver,
100 silver = 1 gold.
Auction House (AH) - Place where people sell their goods in-game.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43560) of an [OpenML dataset](https://www.openml.org/d/43560). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43560/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43560/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43560/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

